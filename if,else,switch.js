// ПРИКЛАД IF З ОПЕРАТОРОМ "!"
/* const person = {
  age: 20
}

if (!person.name) {
  console.log('Имя не указано')
} */






/* IF ELSE
let val = 10
if (val < 5) {
  val += 20
} else {
  val -= 20
}
console.log(val) // -10
*/





/* IF ELSE IF
const age = 25
if (age > 18) {
  console.log('Is adult')
} else if (age >= 12) {
  console.log('Is teenager')
} else {
  console.log('Is child');
}

АЛЕ КРАЩЕ ВИКОРИСТОВУВАТИ
const age = 25
if (age >= 18) {
  console.log('Is adult');
}
if (age >= 12 && age < 18) {
  console.log('Is teenager');
}
if (age < 12) {
  console.log('Is child');
}
*/





/* IF в функціях
const sumPositiveNumbers = (a, b) => {
  if (typeof a !== 'number' || typeof b !== 'number') {
    return 'One of the arguments is not a number'
  }
  if (a <= 0 || b <= 0) {
    return 'Numbers are not positive'
  }
  return a + b
}
console.log(sumPositiveNumbers('Sasha', false)) // One of the arguments is not a number
console.log(sumPositiveNumbers(-10, 8)) // Numbers are not positive
console.log(sumPositiveNumbers(3, 7)) // 10
*/





/* SWITCH
const age = 23
switch (age) {
  case 24:
    console.log('Мені 24 роки')
    break
  case 23:
    console.log('Наді 23 роки')
    break
  case 33:
    console.log('Віталію 33 роки')
    break
  default:
    console.log('Це не наша компанія')
}
*/