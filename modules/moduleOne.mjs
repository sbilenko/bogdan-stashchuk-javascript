const diff = (a, b) => a - b
const div = (a, b) => a / b 
 
export {
  diff,
  div
}

