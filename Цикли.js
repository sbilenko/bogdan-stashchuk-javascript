/*ТИПИ ЦИКЛІВ(for, for...in..., while, do...while, for...of...)
FOR*
 for (let i = 0; i < 5; i++) {
  console.log(i)
}
*/


/*const myArray = ['first', 'second', 'third']
for (let i = 0; i < myArray.length; i++) {
  console.log(myArray[i])
}
*/


/* FOREACH*
const myArray = ['first', 'second', 'third']
myArray.forEach((element, index) => {
  console.log(element, index)
})
// 'first'
// 'second'
// 'third'
*/


/* WHILE
let i = 0
while (i < 5) {
  console.log(i)
  i++
}
// 0, 1, 2, 3, 4
*/


/* DO WHILE
let i = 0
do {
  console.log(i)
  i++
} while (i < 5)
// 0, 1, 2, 3, 4
*/


/* KEYS FOREACH для об'єктів (трансформує значення об'єкту в новий масив і перебирає їх)
const myObject = {
  x: 10,
  y: true,
  z: 'abc'
}

Object.keys(myObject).forEach(key => {
  console.log(key, myObject[key])
})
*/


/* VALUES FOREACH для об'єктів (трансформує значення об'єкту в новий масив і перебирає їх)
const myObject = {
  x: 10,
  y: true,
  z: 'abc'
}

Object.values(myObject).forEach(value => {
  console.log(value)
})
*/


/* FOR IN для масивів(НЕ БАЖАНО ВИКОРИСТОВУВАТИ, краще рідний FOREACH)
const myArray = [true, 10, 'abc', null]

for (const key in myArray) {
  console.log(myArray[key])
} */


/* FOR OF для рядків
const myString = 'Nadia Naichuk is my love'

for(const letter of myString) {
  console.log(letter)
} */


/* FOR OF для масивів(НЕ БАЖАНО ВИКОРИСТОВУВАТИ, Краще рідний FOREACH)
const myArray = [true, 10, 'abc', null]

for (const element of myArray) {
  console.log(element)
} */


/* FOR OF ДЛЯ ОБ'ЄКТІВ(НЕ МОЖНА ВИКОРИСТОВУВАТИ, тому що FOR OF не знає, в якій послідовності перебирати властивості об'єкта. Треба використовувати FOR IN, або методи Object.keys або Object.values для об'єктів)
let myObject = {
  x: 10,
  y: true,
  z: 'abc'
}

for (const prop of myObject) {
  console.log(prop)
}
// myObject is not iterable
 */