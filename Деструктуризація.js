/* ДЕСТРУКТУРИЗАЦІЯ ОБ'ЄКТІВ
const myProfile = {
  name: 'Sasha',
  age: 24,
  isMarried: false
}

let {name, age, isMarried} = myProfile
age = 74
console.log(age)
console.log(isMarried) // 74,  false
*/





/* ДЕСТРУКТУРИЗАЦІЯ МАСИВІВ
const fruits = ['Apple', 'Banana']
const [fruitOne, fruitTwo] = fruits
console.log(fruitOne)
console.log(fruitTwo)
*/





/* ДЕСТРУКУРИЗАЦІЯ В ФУНКЦІЇ
const userProfile = {
  name: 'Sasha',
  age: 24,
  isMarried: false
}

const userInfo =({name, age}) => {
  if(!age) {
    return `User ${name} died.`
  }
  return `User ${name} has ${age}.`
}

console.log(userInfo(userProfile))
*/