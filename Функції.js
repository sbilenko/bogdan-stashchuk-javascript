'use strict'
/* АНОНІМНИЙ ФУНКЦІОНАЛЬНИЙ ВИРАЗ
const a = function(value, multiplier = 2) {
  return value * multiplier
}
console.log(a(2, 3))
*/





/* СТРІЛОЧНА ФУНКЦІЯ
const a = (value, multiplier) => {
  return value * multiplier
}
console.log(a(2, 10))
*/





/* НЕЯВНЕ ПОВЕРНЕННЯ ОБ'ЄКТУ(БЕЗ RETURN)(ДОДАВАННЯ НОВОГО)
const newPost = (post, addedAt = Date()) => ({
  ...post,
  addedAt
})

const firstPost = {
  id: 1,
  author: 'Sasha'
}

console.log(newPost(firstPost)) // {id: 1, author: 'Sasha', addedAt: 'Wed Jul 13 2022 13:41:37 GMT+0300 (за східноєвропейським літнім часом)'}
*/





/* ЯВНЕ ПОВЕРНЕННЯ ОБ'ЄКТУ(З RETURN)
const newPost = (post, addedAt = Date()) => ({
  return: {
  ...post,
  addedAt
  }
})

const firstPost = {
  id: 1,
  author: 'Sasha'
}

console.log(newPost(firstPost)) // {return: {id: 1, author: 'Sasha', addedAt: 'Wed Jul 13 2022 13:48:34 GMT+0300 (за східноєвропейським літнім часом)'}}
*/





/* const personOne = {       // Не бажано мутувати зовнішній об'єкт всередині ф-ії.
  name: 'Sasha',                Краще створювати копію об'єкта всередині ф-ії(див. приклад нижче)
  age: 24,
  city: 'Kyiv'
}

function changePerson(person) {
  person.name = 'Nadia'
  person.age -= 1
  return person
}

changePerson(personOne)

console.log(personOne) */





/* const sasha = {
  mother: 'Alla',
  father: 'Ihor',
  brother: 'Vitya',
  sister: 'Alina'
}

function changeRelatives(person) {
  const newPerson = Object.assign({}, person)
  person.mother = 'Tetiana'
  person.father = 'Vasil'
  delete person.brother
  person.sister = 'Anastasiya'
  return newPerson
}

const nadia = changeRelatives(sasha)

console.log(nadia)

console.log(sasha) */





// Callback Function
/* function myFn() {
  return 'Hey guys'
}

function callMyFn(anotherFunction) {
  return anotherFunction()
}

const newMyFn = callMyFn(myFn)

console.log(newMyFn) */





/* let a
let b

function myFn() {      // Не рекомендується змінювати значення змінних в функції, ПОТРІБНО створювати нові змінні в функції і працювати з ними (Неправильний приклад). Див. правильний приклад нижче.
  let b
  a = true
  b = 10
  console.log(a)
}

myFn()

console.log(a)
console.log(b) */





/* let a = false
let b = 8

function myFn() {         // Створювати нові змінні(let, const) в ф-ії тільки, якщо вони не задані в параметрах ф-ії "()"
  let a = true
  let b = 12
  console.log(a)
  console.log(b)
}

myFn()

console.log(a)
console.log(b) */





/* function myFn() {          // Не рекомендується присвоювати значення змінній(а) не оголосивши її раніше в локальній/глобальній області видимості. 
  a = true                 // Але код не видасть помилку. Змінна(а) буде автоматично оголошена в    глобальній області видимості.
  console.log(a)           // Можна використовувати 'use strict' для заборони використання неоголошених змінних(Використовується на початку сторінки, або на початку тіла функції).
}

myFn()

console.log(a) */





// Анонімна ф-ія
/* let counter = 0
const interval = setInterval(function () {
  if(counter === 5) {
    clearInterval(interval)
  } else {
    console.log(++counter)
  }
  
}, 1000); */



function sumAll(...all) {
  let result = 0
  for (let num of all) {
    result += num
  }
  return result
}

let sum = sumAll(5, 6, 7, 8, 9, 1, 2, 3, 4, 5)
console.log(sum)


