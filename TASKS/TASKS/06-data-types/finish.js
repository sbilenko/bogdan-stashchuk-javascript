/** ЗАДАЧА 6 - Типы данных
 *
 * 1. Объявите несколько переменных и присвойте им значения:
 *  - строка
 *  - число
 *  - логическое
 *  - null
 *  - undefined
 *  - объект
 *  - массив
 *
 * 2. Выведите в консоль тип каждого из значений
 * используя оператор typeof
 */



const str = 'Sasha'
const num = 7
const bool = true
const aNull = null
let undfn
let obj = {}
let arr = []

console.log(typeof str)
console.log(typeof num)
console.log(typeof bool)
console.log(typeof aNull)
console.log(typeof undfn)
console.log(typeof obj)
console.log(typeof arr)