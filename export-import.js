/* // МОДУЛІ(export...; import ... from...)
// Експорт
const a = 7
const b = 'Sasha'

export {
  a,
  b
}
// Імпорт
import {
  a as modifiedA,
  b
} from './moduleOne.mjs'


console.log(modifiedA)
console.log(b)
*/