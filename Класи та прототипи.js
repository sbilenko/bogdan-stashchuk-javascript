// КЛАСИ ТА ПРОТОТИПИ
// ЗВЕРНУТИ УВАГУ!!!
/* class Person {
  constructor(userName, age, profession, userCity) {
    this.userName = userName
    this.age = age
    this.profession = profession
    this.userCity = userCity
  }

  greeting(name) {    // Коли описуємо загальний метод для об'єктів, "function" не пишемо
    console.log(`Привіт, ${name}! Мене звати ${this.userName}.`)
  }
} 

const person1 = new Person('Oleksandr', 24, 'programmer', 'Kyiv')
const person2 = new Person('Nadiya', 23, 'lashmaker', 'Kyiv')
person1.greeting('друже')
person2.greeting('друже')
*/





class Comment {
  constructor(text) {
    this.text = text
    this.votesQty = 0 
  }

  upvote() {
    this.votesQty += 1
  }
}

const firstComment = new Comment('First comment')

// firstComment.upvote()
// console.log(firstComment.votesQty)
// firstComment.upvote()
// console.log(firstComment.votesQty)

console.log(firstComment.hasOwnProperty('text'))
console.log(firstComment.hasOwnProperty('votesQty'))
console.log(firstComment.hasOwnProperty('upvote'))
console.log(firstComment.hasOwnProperty('hasOwnProperty'))






// СТВОРЕННЯ ДЕКІЛЬКОХ ЕКЗЕМПЛЯРІВ
/* class Comment {
  constructor(text) {
    this.text = text
    this.votesQty = 0
  }

  upvote() {
    this.votesQty += 1
  }
}

const firstComment = new Comment('First comment')
const secondComment = new Comment('Second comment')
const thirdComment = new Comment('Thi rd comment')
*/





// СТАТИЧНИЙ МЕТОД
/* class Comment {
  constructor(text) {
    this.text = text
    this.votesQty = 0
  }

  upvote() {
    this.votesQty += 1
  }

  static mergeComments(first, second) {
    return `${first} ${second}`
  }
}

Comment.mergeComments('Раз', 'Два')
*/



