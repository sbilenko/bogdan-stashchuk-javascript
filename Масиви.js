/* ФОРМАТ ЗАПИСУ МАСИВІВ

const myArray = [1, 2, 3]
console.log(myArray)

const myArray2 = new Array(1, 2, 3)
console.log(myArray2)
*/





/* МЕТОДИ МАСИВІВ
PUSH(+ В КІНЦІ МАСИВУ)
const myArray = [1, 2, 3]
console.log(myArray) // [ 1, 2, 3 ]
myArray.push('Sasha') // [ 1, 2, 3, 'Sasha' ]
console.log(myArray)


POP(- В КІНЦІ МАСИВУ)
const myArray = [1, 2, 3]
console.log(myArray) // [ 1, 2, 3 ]
myArray.pop()
console.log(myArray) // [ 1, 2 ]
const removedElement = myArray.pop()
console.log(removedElement) // ВІДОБРАЖЕННЯ ОСТАННЬОГО ЕЛЕМЕНТУ, ЯКИЙ БУВ ВИДАЛЕНИЙ


UNSHIFT(+ НА ПОЧАТОК МАСИВУ)
const myArray = [1, 2, 3]
console.log(myArray) // [ 1, 2, 3 ]
myArray.unshift(true)
console.log(myArray) // [ true, 1, 2, 3 ]
myArray.unshift(24)
console.log(myArray) // [ 24, true, 1, 2, 3 ]


SHIFT(- НА ПОЧАТКУ МАСИВУ)
const myArray = [1, 2, 3]
console.log(myArray) // [ 1, 2, 3 ]
myArray.shift()
console.log(myArray) // [ 2, 3 ]
const removedElement = myArray.shift()
console.log(removedElement) // ВІДОБРАЖЕННЯ ПЕРШОГО ЕЛЕМЕНТУ, ЯКИЙ БУВ ВИДАЛЕНИЙ


FOREACH(Не створює новий масив, просто виконує дії з елементами)
const myArray = [3, 4, 5]
console.log(myArray) // [ 3, 4, 5 ]
myArray.forEach(el => console.log(el*3)) // 9, 12, 15
console.log(myArray) // [ 3, 4, 5 ] ОРИГІНАЛЬНИЙ МАСИВ НЕ ЗМІНИВСЯ


MAP(Створює повністю новий масив)
const myArray = [3, 4, 5]
console.log(myArray) // [ 3, 4, 5 ]
const newArray = myArray.map(el => el*3)
console.log(newArray) // [ 9, 12, 15 ] СТВОРЕНИЙ ПОВНІСТЮ НОВИЙ МАСИВ
console.log(myArray) // [ 3, 4, 5 ] ОРИГІНАЛЬНИЙ МАСИВ НЕ ЗМІНИВСЯ
*/





/* ДОДАВАННЯ І ВІДНІМАННЯ ЕЛЕМЕНТІВ МАСИВУ
const arr = new Array('Jazz', 'Blues')

arr.push('Rock-n-Roll')

arr[Math.floor((arr.length - 1) / 2)] = "Classics" // Код шукає медіанний(рівно серединний) елемент у масиві. // Інший варіант - arr.splice(1, 1, 'Classics')

console.log(arr.shift())

arr.unshift('Rap', 'Reggae')

console.log(arr) 
 */