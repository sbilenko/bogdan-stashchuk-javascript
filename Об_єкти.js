const person1 = {
  name: 'Alex',
  age: 24,
  city: 'Kyiv'
}

const person2 = JSON.parse(JSON.stringify(person1))
person2.name = 'Vitaliy'
person2.age = 30
console.log(person1)
console.log(person2)





/* КОПІЮВАННЯ ПОЧАТКОВОГО ОБ'ЄКТУ(person) БЕЗ ВКЛАДЕНИХ В НЬОГО ОБ'ЄКТІВ

вар1 - const person2 = Object.assign({}, person);

вар2 - const person2 = {...person};
*/





/* ПОВНЕ КОПІЮВАННЯ ПОЧАТКОВОГО ОБ'ЄКТУ(person)

const person2 = JSON.parse(JSON.stringify(peson))
*/

/* const nadia = {
  name: 'Nadia',
  age: 23,
  city: 'Kyiv'
}

const sasha = Object.assign({}, nadia)
sasha.age = 24,
sasha.name = 'Sasha'

console.log(sasha)
console.log(nadia)
*/





/* СТВОРЕННЯ КОПІЇ ПЕРШОГО ОБ'ЄКТУ(Новий об'єкт змінюється, оригінал не змінюється)
const object = {
  name: 'Sasha',
  age: 24,
  city: 'Kyiv',
  weather: 'cold'
}
const object2 = { ...object }

object2.age = 30
object2.sport = 'football'

console.log(object) // { name: 'Sasha', age: 24, city: 'Kyiv', weather: 'cold' }
console.log(object2) // {name: 'Sasha', age: 30, city: 'Kyiv', weather: 'cold', sport: 'football'}
*/





/* ПЕРЕДАВАТИ І ЗМІНЮВАТИ ОБ'ЄКТИ ВСЕРЕДИНІ ФУНКЦІЇ

const personSasha = {
  name:  'Sasha',
  age: 24
}

function func(person) {
  const newPerson = {...person}
  newPerson.name = 'Nadia'
  newPerson.age = 23
  return newPerson
}

const personNadia = func(personSasha)
console.log(personSasha.name)
console.log(personSasha.age)
console.log(personNadia.name)
console.log(personNadia.age)
*/